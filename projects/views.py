from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from tasks.models import Task

# Create your views here.


@login_required
def list_projects(request):
    all_my_projects = Project.objects.filter(owner=request.user)
    context = {"all_my_projects": all_my_projects}
    return render(request, "projects/list_projects.html", context)


@login_required
def project_details(request, id):
    project_info = Task.objects.filter(project=id)
    spec_project = Project.objects.get(id=id)
    context = {"project_detail": project_info, "project": spec_project}
    return render(request, "projects/show_project.html", context)


@login_required
def new_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
