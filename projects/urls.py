from django.urls import path
from projects.views import list_projects, project_details, new_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", new_project, name="create_project"),
]
