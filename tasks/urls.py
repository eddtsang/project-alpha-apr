from django.urls import path
from tasks.views import new_task, my_tasks

urlpatterns = [
    path("create/", new_task, name="create_task"),
    path("mine/", my_tasks, name="show_my_tasks"),
]
